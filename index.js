const express = require("express")

const app = express()
const PORT = 8080

app.get('/',(req,res) => {
	return res.json({message: "Successful v1"})
})

app.listen(PORT,() => console.log(`Server is Up and running on PORT ${PORT}`))
